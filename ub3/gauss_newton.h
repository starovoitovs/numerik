#include "../lib/matrix/matrix.h"

#ifndef TOLERANCE
#define TOLERANCE = 1e-8;
#endif

Matrix gauss_newton(Matrix(*)(Matrix), Matrix(*)(Matrix), Matrix x, double tolerance);