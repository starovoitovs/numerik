#include "../lib/matrix/matrix.h"
#include "gauss_newton.h"
#include <iostream>
#include <stdexcept>

/**
 * Gauss Newton Verfahren. Hier ist alles klar, was gemacht wird.
 */
Matrix gauss_newton(Matrix(*phi)(Matrix), Matrix(*Dphi)(Matrix), Matrix x, double tolerance) {

	Matrix derivative = Dphi(x);

	while((derivative.transpose() * phi(x)).norm() > tolerance) {
		x = x + linear_solve(derivative.transpose() * derivative, derivative.transpose() * phi(x) * (-1));
		derivative = Dphi(x);
	}

	return x;

}