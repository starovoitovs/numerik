#include <stdexcept>
#include <cmath>
#include <iostream>
#include <stdlib.h>
#include "../lib/matrix/matrix.h"
#include "../lib/num/num.h"
#include "bfgs.h"

/**
 * Übergebe Funktion, Ableitung, Startwert, Fehlertoleranz und Startmatrix
 */
Matrix bfgs(double(*f)(Matrix), Matrix(*Df)(Matrix), Matrix x, double tolerance, Matrix H) {

	/* Falls kein Vektor in der Eingabe, Exception */
	if(x.cols() != 1) {
		throw std::runtime_error("Function not defined properly");
	}

	unsigned int n = x.rows();

	bool c1, c2;
	double t, gamma;

	Matrix gr(0, 0),
		direction(0, 0),
		direction_transpose(0, 0),
		sk(0, 0),
		yk(0, 0),
		id(n, n),
		_x(0, 0);

	id.identity();

	/* Berechne Gradient am Anfang */
	gr = Df(x);

	/* Solange die Norm größer als Toleranz */
	while(gr.norm() > TOLERANCE) {

		/* Rechne Gradient aus */
		gr = Df(x);
		/* Rechne Richtung aus */
		direction = H * gr * (-1);

		/* Armijo Bedingungen */
		t = 2; direction_transpose = direction.transpose();

		/* Halbiere t, solange beide nicht erfüllt */
		do {
			t /= 2;
			c1 = f(x + direction * t) > f(x) + MU1 * t * ((direction_transpose * gr)[0][0]);
			c2 = std::abs((direction_transpose * Df(x + direction * t))[0][0]) > MU2 * std::abs((direction_transpose * gr)[0][0]);
		} while(c1);

		/* Aktualisiere x */
		_x = x; // x_k
		x = x + direction * t; // x_k+1

		sk = x - _x; // x_k+1 - x_k
		yk = Df(x) - gr; // gr(f(x_k+1)) - gr(f(x_k))

		/* Falls im Nenner steht 0, ist gamma nicht berechenbar, breche ab */
		if((yk.transpose() * sk)[0][0] == 0) {
			return x;
		}

		/* Aktualisiere H */
		gamma = 1 / (yk.transpose() * sk)[0][0];
		H = (id - sk * yk.transpose() * gamma) * H * (id - yk * sk.transpose() * gamma) + sk * sk.transpose() * gamma;

	}

	return x;
	
}
