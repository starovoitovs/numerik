#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <functional>
#include "stdlib.h"
#include "../lib/matrix/matrix.h"
#include "../lib/num/num.h"
#include "bfgs.h"
#include "gauss_newton.h"

/**
 * Fehlertoleranz
 */
#ifndef TOLERANCE
#define TOLERANCE 1e-8
#endif

/* Werte zum anpassen */
double args[7] = {30, 125, 225, 367, 535, 740, 990};
double vals[7] = {95, 90, 85, 80, 75, 70, 65};

/**
 * Die Funktion f, die wir bei BFGS anwenden. Wir minimieren die Summe der Quadrate
 */
double f(Matrix x) {

	double a = x[0][0], b = x[1][0], c = x[2][0];
	double sum = 0;

	/* Wir minimieren die Summe der Quadrate der Fehler */
	for(size_t i = 0; i < 7; i++) {
		sum += pow((a + b * exp(-c * args[i])) - vals[i], 2);
	}

	return sqrt(sum);

}

/**
 * Explizit gegebene Ableitung der Funktion f von oben.
 */
Matrix Df(Matrix x) {

	double a = x[0][0], b = x[1][0], c = x[2][0];

	double Da = 0;
	double Db = 0;
	double Dc = 0;
	double p;

	for(size_t i = 0; i < 7; i++) {

		p = (-2) * (vals[i] - (a + b * exp(-c * args[i])));

		Da += p;
		Db += p * exp(-c * args[i]);
		Dc += p * (-b) * args[i] * exp(-c * args[i]);

	}

	Vector gradient(3);

	gradient[0][0] = Da;
	gradient[1][0] = Db;
	gradient[2][0] = Dc;

	return gradient;

}

/**
 * Explizit gegebene Hesse-Matrix der Funktion f. Wird eigentlich hier in dieser Aufgabe nicht benutzt,
 * ich nehme Identitätsmatrix am Anfang.
 */
Matrix Hf(Matrix x) {

	double a = x[0][0], b = x[1][0], c = x[2][0];
	double p, q;

	Matrix hessian(3, 3);

	for(int i = 0; i < 7; i++) {
		p = exp(-c * args[i]);
		q = exp(-c * 2 * args[i]);
		hessian[0][0] += 2;
		hessian[1][1] += 2 * q;
		hessian[2][2] += 2 * vals[i] * b * args[i] * (-args[i]) * p  - 2 * a * b * args[i] * (-args[i]) * p - b * b * args[i] * (-2) * args[i] * q;
		hessian[0][1] += 2 * p;
		hessian[1][0] += 2 * p;
		hessian[1][2] += 2 * vals[i] * args[i] * p - 2 * a * args[i] * p - 4 * b * args[i] * q;
		hessian[2][1] += 2 * vals[i] * args[i] * p - 2 * a * args[i] * p - 4 * b * args[i] * q;
		hessian[2][0] += 2 * b * (-args[i]) * p;
		hessian[0][2] += 2 * b * (-args[i]) * p;
	}

	return hessian;

}

/**
 * Funktion phi, deren Norm wir minimieren in Gauss-Newton Verfahren.
 */
Matrix phi(Matrix x) {

	Vector val(7);

	double a = x[0][0], b = x[1][0], c = x[2][0];

	for(int i = 0; i < 7; i++) {
		val[i][0] = vals[i] - (a + b * exp( -c * args[i] ) );
	}

	return val;

}

/**
 * Ableitung der Funktion phi.
 */
Matrix Dphi(Matrix x) {

	double a = x[0][0], b = x[1][0], c = x[2][0];

	Matrix jacobian(7, 3);

	for(int i = 0; i < 7; i++) {
		jacobian[i][0] = -1;
		jacobian[i][1] = -exp(-c * args[i]);
		jacobian[i][2] = b * args[i] * exp(-c * args[i]);
	}

	return jacobian;

}

/**
 * In der Aufgabe sollen wir bestimmen, wann kann der Tee getrunken werden. 
 * Newton-Raphson Verfahren.
 */
double newton(double a, double b, double c) {

	double t = 990;

	while(std::abs(a + b * exp(-c * t) - 60) > TOLERANCE) {
		t = t + (a + b * exp(-c * t) - 60) / (c * b * exp(-c * t));
	}

	return t;

}

/**
 * Wir wollen überprüfen, ob unser Ergebnis x Sinn macht, also hier werden Werte für x ausgegeben.
 */
void test(Matrix x) {

	std::cout << std::endl;
	std::cout << "x:" << std::endl << x;
	std::cout << std::endl;
	std::cout << "f(x):" << std::endl << f(x) << std::endl;
	std::cout << std::endl;
	std::cout << "gradient:" << std::endl << Df(x);
	std::cout << std::endl;
	std::cout << "hessian:" << std::endl << hessian(f, x);
	std::cout << std::endl;

	double a = x[0][0];
	double b = x[1][0];
	double c = x[2][0];

	std::cout << "Newton for T(t) = 60, t = " << newton(a, b, c) << std::endl << std::endl;

	for(int i = 0; i < 7; i++) {
		std::cout << args[i] << "\t" << vals[i] << "\t" << a + b * exp(-c * args[i]) << std::endl;
	}

	std::cout << std::endl;

}

/**
 * Main.
 */
int main(int argc, char * argv[]) {

	std::cout << std::setprecision(10);

	if(argc != 4) {
		throw std::invalid_argument("3 arguments expected.");
	}

	Vector start(3);

	/**
	 * Konversion der Argumente in die Zahlen.
	 */
	for(int i = 1; i < argc; i++) {
		start[i-1][0] = atof(argv[i]);
	}

	/**
	 * BFGS Verfahren. Wir fangen mit der Id an. 
	 */
	Matrix H(3, 3);
	H.identity();

	Matrix x = bfgs(f, Df, start, TOLERANCE, H);
	test(x);

	/**
	 * Gauss-Newton Verfahren.
	 */
	Matrix x = gauss_newton(phi, Dphi, start, TOLERANCE);
	test(x);

	return 0;

}