#ifndef __BFGS_H__
#define __BFGS_H__

#include "../lib/matrix/matrix.h"

/**
 * Fehlertoleranz
 */
#ifndef TOLERANCE
#define TOLERANCE 1e-8
#endif

/**
 * Wolfe Parameter
 */
const double MU1 = 1e-4;
const double MU2 = 1e-1;

Matrix bfgs(double(*)(Matrix), Matrix(*)(Matrix), Matrix x, double tolerance, Matrix H);

#endif/*__BFGS_H__*/