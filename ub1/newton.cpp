#include <iostream>
#include <cmath>
#include <ctime>

double newton1(double x, int &k) {

	while(atan(x) != 0) {
		
		if(std::isinf(x)) {
			throw k;
		}

		x = x - atan(x) * (1 + x*x);

		k++;

	}

	return x;

}

double newton2(double x, double tau, int &k) {

	double p, t;

	while(atan(x) != 0) {

		if(std::isinf(x)) {
			throw k;
		}

		p = - atan(x) * (1 + x*x);
		t = 1;

		while(std::abs(atan(x + p * t)) >= (1 - tau * t) * std::abs(atan(x)) ) {
			t /= 2;
		}

		x = x + p * t;

		k++;

	}

	return x;

}

int main() {

	double values[4][2], diff, x, tau;
	int k;

	values[0][0] = .5; values[0][1] = .5;
	values[1][0] = .5; values[1][1] = .9;
	values[2][0] = 1.5; values[2][1] = .5;
	values[3][0] = 1.5; values[3][1] = .9;

	for(int i = 0; i < 4; i++) {

		x = values[i][0];
		tau = values[i][1];

		k = 0;

		try {
			newton1(x, k);
			std::cout << "Simple Newton method converges for x = " << x << ", tau = " << tau << " in " << k << " iterations." << std::endl;
		} catch (int k) {
			std::cout << "Simple Newton method diverges for x = " << x << ", tau = " << tau << " in " << k << " iterations." << std::endl;
		}

		k = 0;

		try {
			newton2(x, tau, k);
			std::cout << "Damped Netwon method converges for x = " << x << ", tau = " << tau << " in " << k << " iterations." << std::endl;
		} catch (int k) {
			std::cout << "Damped Netwon method diverges for x = " << x << ", tau = " << tau << " in " << k << " iterations." << std::endl;
		}

		std::cout << std::endl;

	}

	return 0;

}