#include <iostream>
#include <iomanip>
#include "../lib/matrix/matrix.h"
#include "quadratic.h"
#include "active_constraints.h"
#include "sqp.h"
#include "stdio.h"
#include "stdlib.h"
#include "initial_sqp_values_penalty.h"
#include <ctime>

/**
 * Test as in example 6.24
 */
void test_active_constraints() {

	Matrix H(2, 2), A(2, 5), g(2, 1), b(5, 1), x(2, 1);
	/**
	 * Define initial values
	 */

	H[0][0] = 2; H[1][1] = 2;

	g[0][0] = -2; g[1][0] = -5;
	
	A[0][0] = -1; A[1][0] = 2;
	A[0][1] = 1; A[1][1] = 2;
	A[0][2] = 1; A[1][2] = -2;
	A[0][3] = -1; A[1][3] = 0;
	A[0][4] = 0; A[1][4] = -1;

	b[0][0] = 2; b[1][0] = 6; b[2][0] = 2; b[3][0] = 0; b[4][0] = 0;

	x[0][0] = 2; x[1][0] = 0;

	std::vector<bool> constraints(A.cols(), false);
	constraints[2] = true;
	constraints[4] = true;

	std::cout << active_constraints(constraints, x, A, b, H, g);

}

void test_sqp() {

	Matrix x(2, 1);
	x[0][0] = .5; x[1][0] = 1;

	Matrix lambda(2, 1);

	std::vector<double(*)(Matrix)> constraints(2);
	constraints[0] = g1;
	constraints[1] = g2;

	std::vector<Matrix(*)(Matrix)> constraint_gradients(2);
	constraint_gradients[0] = gradient_g1;
	constraint_gradients[1] = gradient_g2;

	sqp(W, gradient_f, constraints, constraint_gradients, x, lambda);
	
}

int main() {

	srand(time(NULL));
	std::cout << std::setprecision(2);

	test_sqp();

	return 0;

}