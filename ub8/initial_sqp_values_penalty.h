Matrix W(Matrix x, double rho);
Matrix gradient_f(Matrix x, double rho);
double g1(Matrix x);
double g2(Matrix x);
Matrix gradient_g1(Matrix x);
Matrix gradient_g2(Matrix x);