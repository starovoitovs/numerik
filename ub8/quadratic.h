/**
 * Solving quadratic problem like (6.15)
 */

#include "../lib/matrix/matrix.h"

Matrix quadratic(Matrix H, Matrix g, Matrix A, Matrix b);