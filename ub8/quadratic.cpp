#include "quadratic.h"
#include "../lib/matrix/matrix.h"
#include <iostream>

Matrix quadratic(Matrix H, Matrix g, Matrix A, Matrix b) {

	/**
	 * Construct block Matrix from A and H
	 */

	unsigned int m = H.rows() + A.cols();
	unsigned int n = H.cols() + A.cols();

	Matrix B(m, n);

	for(size_t i = 0; i < B.rows(); i++) {
		for(size_t j = 0; j < B.cols(); j++) {
			if(i < H.rows()) {
				if(j < H.cols()) {
					B[i][j] = H[i][j];
				} else {
					B[i][j] = A[i][j - H.cols()];
				}
			} else {
				if(j < A.rows()) {
					B[i][j] = A[j][i - H.rows()];
				}
			}
		}
	}

	/**
	 * Construct block vector
	 */

	Matrix C(H.rows() + A.cols(), 1);

	for(size_t i = 0; i < C.rows(); i++) {
		if(i < H.rows()) {
			C[i][0] = (-1) * g[i][0];
		} else {
			C[i][0] = b[i - H.rows()][0];
		}
	}

	/**
	 * Solve linear equation system
	 */

	Matrix solution = linear_solve(B, C);
	Matrix result(H.rows(), 1);

	for(int i = 0; i < H.rows(); i++) {
		result[i][0] = solution[i][0];
	}

	return solution;

}