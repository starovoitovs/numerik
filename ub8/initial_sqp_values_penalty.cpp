#include "../lib/matrix/matrix.h"
#include <math.h>


double g1(Matrix x) {

	return (-1) * x[0][0] * x[0][0] + x[1][0];

}

double g2(Matrix x) {

	return (-1) * x[0][0] * x[0][0] - x[1][0] * x[1][0] + 1;

}

Matrix W(Matrix x, double rho) {

	double x1 = x[0][0], x2 = x[1][0];

	double A = (-4*x1*x2+24*x2-24*x1) / (4*x1*x2 + 2*x1);
	double D1A = (-16*x1*x2 -96*x1 +48*x2 -96*x2*x2) / pow(4*x1*x2 + 2*x1, 2);
	double D2A = ( (-4*x1+24)*(4*x1*x2+2*x1) - 4*x1*(-4*x1*x2+24*x2-24*x1) ) / pow(4*x1*x2 + 2*x1, 2);
	double D11A = ( (-16*x2-96) * pow(4*x1*x2+2*x1,2) - 2*(4*x1*x2+2*x1)*(4*x2+2)*(-16*x1*x1-96*x1+48*x2-96*x2*x2) ) / pow(4*x1*x2+2*x1, 4);
	double D12A = ( (-16*x1+48-96*x2)*pow(4*x1*x2+2*x1, 2) - 2*(4*x1*x2+2*x1)*(4*x2+2)*(-16*x1*x2-96*x1+48*x2-96*x2*x2) ) / pow(4*x1*x2+2*x1, 4);
	double D22A = (-2)*4*x1*(88*x1*x1+48*x1) / pow(4*x1*x2+2*x1, 3);

	double B = (-8*x1*x2+18*x1+12) / (4*x1*x2+2*x1);
	double D1B = (-48*x2-24) / pow(4*x1*x2+2*x1, 2);
	double D2B = (-88*x1*x1-48*x1) / pow(4*x1*x2+2*x1, 2);
	double D11B = (-48*x2-24)*(-2)*(4*x2+2) / pow(4*x1*x2+2*x1, 3);
	double D12B = -48*pow(4*x1*x2+2*x1, 2) - 2*(4*x1*x2+2*x1)*4*x1*(-48*x2-24);
	double D22B = (-88*x1*x1-48*x1)*(-2)*4*x1 / pow(4*x1*x2+2*x1, 3);

	Matrix result(2, 2);
	result[0][0] = 6;
	result[1][1] = 4;

	if(g1(x) < A / rho) {
		result[0][0] += -2*(-A+rho/2) + (-D1A)*(-2*x1) + (-D11A)*(-x1*x1+x2) + (-2*x1*x1)*(-D1A);
		result[0][1] += -D1A + -D12A*(-x1*x1+x2) + (-2*x1)*(-D2A);
		result[1][0] += -D1A + -D12A*(-x1*x1+x2) + (-2*x1)*(-D2A);
		result[1][1] += -D2A + -D22A*(-x1*x1+x2) + (-2*x1)*(-D2A);
	} else {
		result[0][0] += -1/rho * (D1A*D1A + A*D11A);
		result[0][1] += -1/rho * (D2A*D1A + A*D12A);
		result[1][0] += -1/rho * (D2A*D1A + A*D12A);
		result[1][1] += -1/rho * (D2A*D2A + A*D22A);
	}

	if(g2(x) < B / rho) {
		result[0][0] += (-2)*(-B+rho/2) + -D1B*(-2*x1) + (-D11B)*(-x1*x1-x2*x2+1) + (-2*x1)*(-D1B);
		result[0][1] += -2*x2-D1B + -D12B*(-x1*x1-x2*x2+1) + (-2*x1)*(-D2B);
		result[1][0] += -2*x2-D1B + -D12B*(-x1*x1-x2*x2+1) + (-2*x1)*(-D2B);
		result[1][1] += -2*(-B+rho/2) + -D2B*(-2*x2) + -D22B*(-x1*x1-x2*x2+1) + (-2*x2)*(-D2B);
	} else {
		result[0][0] += -1/rho * (D1B*D1B + B*D11B);
		result[0][1] += -1/rho * (D2B*D1B + B*D12B);
		result[1][0] += -1/rho * (D2B*D1B + B*D12B);
		result[1][1] += -1/rho * (D2B*D2B + B*D22B);
	}

	return result;

}

Matrix gradient_f(Matrix x, double rho) {

	Matrix result(2, 1);
	result[0][0] = 6 * x[0][0] - 12;
	result[1][0] = 4 * x[1][0] - 12;

	return result;

}

Matrix gradient_g1(Matrix x) {

	Matrix result(2, 1);

	result[0][0] = (-2) * x[0][0];
	result[1][0] = 1;

	return result;

}

Matrix gradient_g2(Matrix x) {

	Matrix result(2, 1);

	result[0][0] = (-2) * x[0][0];
	result[1][0] = (-2) * x[1][0];

	return result;

}