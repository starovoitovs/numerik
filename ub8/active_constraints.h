/**
 * Solving be determining active constraints.
 */

#include <vector>
#include "../lib/matrix/matrix.h"

Matrix active_constraints(
	std::vector<bool> constraints,
	Matrix start_value,
	Matrix A,
	Matrix b,
	Matrix H,
	Matrix g
	);