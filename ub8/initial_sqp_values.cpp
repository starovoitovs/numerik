#include "../lib/matrix/matrix.h"

Matrix W(Matrix x, Matrix lambda) {

	Matrix result(2, 2);
	result[0][0] = 6 + 2 * lambda[0][0] + 2 * lambda[1][1];
	result[1][1] = 4 + 2 * lambda[1][1];

	return result;

}

Matrix gradient_f(Matrix x) {

	Matrix result(2, 1);
	result[0][0] = 6 * x[0][0] - 12;
	result[1][0] = 4 * x[1][0] - 12;

	return result;

}

double g1(Matrix x) {

	return (-1) * x[0][0] * x[0][0] + x[1][0];

}

double g2(Matrix x) {

	return (-1) * x[0][0] * x[0][0] - x[1][0] * x[1][0] + 1;

}

Matrix gradient_g1(Matrix x) {

	Matrix result(2, 1);

	result[0][0] = (-2) * x[0][0];
	result[1][0] = 1;

	return result;

}

Matrix gradient_g2(Matrix x) {

	Matrix result(2, 1);

	result[0][0] = (-2) * x[0][0];
	result[1][0] = (-2) * x[1][0];

	return result;

}