#include "../lib/matrix/matrix.h"
#include "active_constraints.h"
#include <iostream>
#include <vector>

Matrix sqp(
	Matrix(*W)(Matrix, double),
	Matrix(*gradient_f)(Matrix, double),
	std::vector<double(*)(Matrix)> constraints,
	std::vector<Matrix(*)(Matrix)> constraint_gradients,
	Matrix x,
	Matrix lambda
	) {

	/**
	 * We can initially know the size of the vector in the variable is by looking at the dimensions of 
	 * Hessian (which is quadratic).
	 */
	unsigned int N = x.rows();

	/**
	 * Initializing vector where we set initial constraints
	 */
	std::vector<bool> constraints_boolean_vector(constraints.size(), false);

	double rho = 1;

	while(true) {

		/**
		 * Initializing matrix that has gradients gi(x_k) in columns
		 */
		Matrix constraints_matrix(N, constraints.size()),
			b(constraints.size(), 1);

		for(int i = 0; i < constraints.size(); i++) {

			Matrix gradient_gi = (constraint_gradients[i])(x);

			for(int j = 0; j < constraints_matrix.rows(); j++) {
				constraints_matrix[j][i] = gradient_gi[j][0] * (-1);
			}

			b[i][0] = (constraints[i])(x);

		}

		Matrix p = active_constraints(constraints_boolean_vector, x, constraints_matrix, b, W(x, rho), gradient_f(x, rho));

		x = x + p;
		rho *= 10;

		double sum = 0;

		for(int i = 0; i < constraints.size(); i++) {
			sum += (constraints[i])(x) * (constraints[i])(x);
		}

		int l;
		std::cin >> l;
		std::cout << x;
		std::cout << std::endl << "diff: " << sum << std::endl;

	}

}