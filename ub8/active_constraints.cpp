#include <vector>
#include "../lib/matrix/matrix.h"
#include "quadratic.h"

/**
 * For constraints we got a boolean of vector of size #constraints, where true is for active constraint.
 * So this function just counts how much TRUE values this vector contains.
 */
unsigned int get_number_of_active_constraints(std::vector<bool> constraints) {
	
	unsigned int number_of_active_constraints = 0;
	for(int i = 0; i < constraints.size(); i++) {
		if(constraints[i]) {
			number_of_active_constraints++;
		}
	}

	return number_of_active_constraints;

}

/**
 * In order to solve quadratic problem, we need to 'slice' matrix and leave only columns corresponding 
 * to active constraints, what this function is designed for.
 */
Matrix initialize_active_constraints_matrix(Matrix A, std::vector<bool> constraints, unsigned int number_of_active_constraints) {

	Matrix active_constraints_matrix(A.rows(), number_of_active_constraints);

	unsigned int current_col = 0,
		current_constraint = 0;

	for(size_t i = 0; i < constraints.size(); i++) {
		
		if(!constraints[i]) {
			continue;
		}

		current_constraint = i;

		for(size_t j = 0; j < A.rows(); j++) {
			active_constraints_matrix[j][current_col] = A[j][current_constraint];
		}

		current_col++;

	}

	return active_constraints_matrix;

}

/**
 * Main function for determining active constraints. 
 */
Matrix active_constraints(
	std::vector<bool> constraints,
	Matrix x,
	Matrix A,
	Matrix b,
	Matrix H,
	Matrix g
	) {

	while(true) {

		/**
		 * Getting the number of active constraints.
		 */
		unsigned int number_of_active_constraints = get_number_of_active_constraints(constraints);

		/**
		 * Getting 'sliced' constraints matrix where columns with active constraints only are left.
		 */
		Matrix active_constraints_matrix =
			initialize_active_constraints_matrix(A, constraints, number_of_active_constraints);

		Matrix h = g + H * x;
		Matrix solution(0, 0);
		
		/**
		 * If there are constraints, we solve constrained quadratic problem.
		 * Solving (6.18), solution is in form (6.16), i.e. vector p (to be added to x) at the top,
		 * and lambda at the bottom.
		 */
		if(number_of_active_constraints > 0) {
			Matrix null_vector(number_of_active_constraints, 1);
			solution = quadratic(H, h, active_constraints_matrix, null_vector);
		}
		/**
		 * If there are no constraints, we just find stationary point by solving Hx = -h.
		 */
		else {
			solution = linear_solve(H, h * (-1));
		}
		
		/**
		 * After we got a solution to our problem, what we want to do is to determine next active 
		 * constraints set.
		 *
		 * If we got at least one negative lambda, then we got a redundant constraint, and we remove
		 * constraint that corresponds with negative lambda of greatest absolute value
		 * from the set of active constraints.
		 */
		
		/**
		 * If this value stays negative, then there is no negative lambda so we're done,
		 * otherwise we store here the index of lambda with negative value of greatest absolute value.
		 **/
		int constraint_to_remove = -1;

		/**
		 * We store current greatest absolute value here to compare with furhter ones.
		 */
		double greatest_negative_lambda_absolute_value = 0;

		Matrix p(x.rows(), 1);

		for(int i = 0; i < solution.rows(); i++) {

			/**
			 * As pointed out, at the top of the solution of quadratic problem we got vector p,
			 * multiple of which we have to add to previous x value. So we 'slice' solution in here
			 * and get p.
			 */
			if(i < x.rows()) {
				p[i][0] = solution[i][0];
				continue;
			}

			/**
			 * If we got here, our current subject is some lambda that corresponds with active 
			 * constraint. In following part of the loop we determine constraint to be removed (might
			 * also be none).
			 */
			if(solution[i][0] < 0 && (-1) * solution[i][0] > greatest_negative_lambda_absolute_value) {
				greatest_negative_lambda_absolute_value = (-1) * solution[i][0];
				constraint_to_remove = i - x.rows();
			}
		}

		/**
		 * When we got constraint to remove, we just do it here.
		 */
		if(constraint_to_remove >= 0) {
			/**
			 * We need this weird loop, because constraint_to_remove corresponds with the index of 
			 * constraint among active constraint, but we don't know the overall index of this constraint,
			 * so we find it here.
			 *
			 * Example: we have active constraints (3, 5). If we want to remove 3, then constraint_to_remove
			 * will equal 0. If we want to remove 5, then constraint_to_remove will equal 1. So following
			 * code would map 0 to 3 and 1 to 5.
			 */
			
			unsigned int c = 0;

			for(int i = 0; i < constraints.size(); i++) {
				if(constraints[i] == true) {
					if(constraint_to_remove == c) {
						constraints[i] = false;
						break;
					} else {
						c++;
					}
				}
			}

			x = x + p;

		}
		/**
		 * If we haven't got any constraint to remove, then we either have to add a constraint or we are
		 * done (see p. 49).
		 */
		else {

			double alpha = 1;

			/**
			 * By analogy with removing active constraints, where we look for 'worst' negative lambda,
			 * here we add active constraint, that is 'the nearest' to current x, index of which will
			 * be stored in constraint_to_add. If it remains -1, then there is no blocking constraint,
			 * so we're done.
			 */
			int constraint_to_add = -1;

			for(int i = 0; i < constraints.size(); i++) {
				
				if(constraints[i] == false) {

					double q = 0;
					
					for(int j = 0; j < A.rows(); j++) {
						q += A[j][i] * p[j][0];
					}

					if(q > 0) {

						double r = 0;
						for(int j = 0; j < A.rows(); j++) {
							r += A[j][i] * x[j][0];
						}

						if( (b[i][0] - r) / q < alpha ) {
							alpha = (b[i][0] - r) / q;
							constraint_to_add = i;
						}
					}

				}

			}

			if(alpha > 1) {
				alpha = 1;
			}

			x = x + p * alpha;

			/**
			 * Means there is blocking constraint so we update our x and set of active constraints.
			 */
			if(constraint_to_add >= 0) {

				constraints[constraint_to_add] = true;

			}
			/**
			 * There are no blocking constraints. We are done.
			 */
			else {

				return x;
				
			}

		}

	}


}