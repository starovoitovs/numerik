#include "../lib/matrix/matrix.h"
#include <vector>

Matrix sqp(
	Matrix(*)(Matrix, double),
	Matrix(*)(Matrix, double),
	std::vector<double(*)(Matrix)> constraints,
	std::vector<Matrix(*)(Matrix)> constraint_gradients,
	Matrix x,
	Matrix lambda
	);