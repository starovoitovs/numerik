#include <iostream>
#include "../lib/matrix/matrix.h"

const double mu = .5;
const double epsilon = 1e-4;

double f(Matrix x) {

	double x1 = x[0][0];
	double x2 = x[1][0];
	double x3 = x[2][0];

	return - x1*x2 - x1*x3 - x2*x3;

}

Matrix gradient(Matrix x) {

	double x1 = x[0][0];
	double x2 = x[1][0];
	double x3 = x[2][0];

	Matrix r = Matrix(3, 1);

	r[0][0] = -x2-x3;
	r[1][0] = -x1-x3;
	r[2][0] = -x1-x2;

	return r;

}

Matrix projection(Matrix x) {

	double x1 = x[0][0];
	double x2 = x[1][0];
	double x3 = x[2][0];

	Matrix normal = Matrix(3, 1);
	normal[0][0] = 1;
	normal[1][0] = 1;
	normal[2][0] = 1;

	double lambda = - (x1 + x2 + x3) / 3 + 1;

	return x + normal * lambda;

}

Matrix optimize_restrictive_projection(
		double(*f)(Matrix),
		Matrix(*gradient)(Matrix),
		Matrix(*projection)(Matrix),
		Matrix start_value ) {

	Matrix x = Matrix(3, 1),
		oldx = Matrix(3, 1),
		direction = Matrix(3, 1);

	x = start_value;

	double t;

	do {

		direction = gradient(x) * (-1);

		t = 1;

		while(f(projection(x + direction * t)) > f(x) - (direction.transpose() * (projection(x + direction * t) - x) * mu )[0][0] ) {
			t /= 2;
		}

		oldx = x;
		x = projection(x + direction * t);

	} while((oldx - x).norm() > epsilon);

	return x;

}


int main() {

	Matrix x = Matrix(3, 1);

	std::cout << optimize_restrictive_projection(f, gradient, projection, x);

	return 0;

}