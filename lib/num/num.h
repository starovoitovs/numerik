/**
 * Numerische Hilfsfunktionen. Implementierung lässt sich verbessern.
 */

#ifndef _NUM_H_
#define _NUM_H_

#include "../matrix/matrix.h"

/* Calculate function gradient with finite differences formula */
Matrix gradient(double(*)(Matrix), Matrix x);

/* Calculate Hessian of a scalar-valued function with finite differences formula */
Matrix hessian(double(*)(Matrix), Matrix x);

#endif