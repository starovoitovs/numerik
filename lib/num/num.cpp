#include <stdexcept>
#include <iostream>
#include "num.h"
#include "../matrix/matrix.h"
#include "float.h"

const double h = 1e-4;

Matrix gradient(double(*f)(Matrix), Matrix x) {

	if(x.cols() != 1) {
		throw std::invalid_argument("Gradient accepts only matrices of columns count 1.");
	}

	Matrix gradient(x.rows(), 1);

	for(size_t i = 0; i < x.rows(); i++) {
		
		Matrix argument(x.rows(), 1);
		for(size_t j = 0; j < x.rows(); j++) {
			argument[j][0] = x[j][0];
		}

		argument[i][0] += h;
		gradient[i][0] = (f(argument) - f(x)) / h;

	}

	return gradient;

}

Matrix hessian(double(*f)(Matrix), Matrix x) {

	if(x.cols() != 1) {
		throw std::invalid_argument("Hessian accepts only matrices of row count 1.");		
	}

	Matrix hessian(x.rows(), x.rows());

	for(int i = 0; i < x.rows(); i++) {
		for(int j = 0; j < x.rows(); j++) {

			Matrix a1(x.rows(), 1), a2(x.rows(), 1), a3(x.rows(), 1);
			for(size_t k = 0; k < x.rows(); k++) {
				a1[k][0] = x[k][0];
				a2[k][0] = x[k][0];
				a3[k][0] = x[k][0];
			}

			a1[i][0] += h;
			a1[j][0] += h;
			a2[i][0] += h;
			a3[j][0] += h;

			hessian[i][j] = (f(a1) - f(a2) - f(a3) + f(x)) / h / h;

		}
	}

	return hessian;

}