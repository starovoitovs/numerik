#include <stdexcept>
#include <iostream>
#include <cmath>
#include "time.h"
#include "stdlib.h"
#include "matrix.h"

const int random_bound = 10;

Matrix::Matrix(unsigned int rows, unsigned int cols) {

	_rows = rows;
	_cols = cols;

	/* Instaniate data */
	_data = (double**) malloc(rows * sizeof(double *));
	for(size_t i = 0; i < rows; i++) {
		_data[i] = (double*) malloc(cols * sizeof(double));
	}

	/* Setup zero matrix */
	for(size_t i = 0; i < _rows; ++i) {
		for(size_t j = 0; j < _cols; ++j) {
			_data[i][j] = 0;
		}
	}

}

unsigned int Matrix::cols() const {
	return _cols;
}

unsigned int Matrix::rows() const {
	return _rows;
}

Matrix Matrix::operator+(Matrix operand) {

	if(_cols != operand.cols() || _rows != operand.rows()) {
		throw std::invalid_argument("Matrix dimensions don't match.");
	}

	Matrix sum(_rows, _cols);

	for(size_t i = 0; i < _rows; i++) {
		for(size_t j = 0; j < _cols; j++) {
			sum._data[i][j] = _data[i][j] + operand._data[i][j];
		}
	}

	return sum;

}

Matrix Matrix::operator-(Matrix operand) {

	return operator+(operand * (-1));

}

Matrix Matrix::operator*(Matrix operand) {

	if(_cols != operand.rows()) {
		throw std::invalid_argument("Matrix dimensions don't match.");	
	}

	Matrix product(_rows, operand.cols());

	for(size_t i = 0; i < _rows; i++) {
		for(size_t j = 0; j < operand.cols(); j++) {
			double sum = 0;
			for(size_t k = 0; k < _cols; k++) {
				sum += _data[i][k] * operand._data[k][j];
			}
			product[i][j] = sum;
		}
	}

	return product;

}

double* Matrix::operator[](unsigned int i) {
	return _data[i];
}

Matrix Matrix::operator*(double scalar) {

	Matrix product(_rows, _cols);
	
	for(size_t i = 0; i < _rows; i++) {
		for(size_t j = 0; j < _cols; j++) {
			product[i][j] = _data[i][j] * scalar;
		}
	}

	return product;

}

Matrix Matrix::transpose() {

	Matrix transposed(_cols, _rows);

	for(size_t i = 0; i < _rows; i++) {
		for(size_t j = 0; j < _cols; j++) {
			transposed[j][i] = _data[i][j];
		}
	}

	return transposed;

}

double Matrix::norm() {

	double norm = 0;

	for(size_t i = 0; i < _rows; i++) {
		for(size_t j = 0; j < _cols; j++) {
			norm += _data[i][j] * _data[i][j];
		}
	}

	return sqrt(norm);

}

Matrix Matrix::copy() {

	Matrix instance(_rows, _cols);
		
	for(int i = 0; i < _rows; i++) {
		for(int j = 0; j < _cols; j++) {
			instance[i][j] = _data[i][j];
		}
	}

	return instance;

}

Matrix Matrix::inverse() {

	if(_rows != _cols) {
		throw std::runtime_error("Can't calculate inverse of non-square matrix");
	}

	/* Working with copied instance */
	Matrix instance = copy();

	/* Resulting matrix */
	Matrix result(_rows, _rows);

	result.identity();

	for(size_t i = 0; i < _rows; i++) {

		if(instance[i][i] == 0) {
			
			size_t j;
			
			for(size_t j = i + 1; j < _rows; j++) {
				if(instance[j][i] != 0) {
					for(size_t k = 0; k < _rows; k++) {
						std::swap(instance[i][k], instance[j][k]);
						std::swap(result[i][k], result[j][k]);
					}
				}
			}

			if(j == _rows) {
				throw std::runtime_error("Matrix is singular");
			}

		}

		for(size_t j = i + 1; j < _rows; j++) {
			
			if(instance[j][i] == 0) {
				continue;
			}

			double a = instance[j][i] / instance[i][i];

			for(size_t k = 0; k < _cols; k++) {

				instance[j][k] -= instance[i][k] * a;
				result[j][k] -= result[i][k] * a;
			}

		}

	}

	for(int i = _rows - 1; i >= 0; i--) {

		if(instance[i][i] == 0) {
			continue;
		}

		for(int j = i - 1; j >= 0; j--) {
			
			if(instance[j][i] == 0) {
				continue;
			}

			double a = instance[j][i] / instance[i][i];

			for(int k = _rows - 1; k >= 0; k--) {

				instance[j][k] -= instance[i][k] * a;
				result[j][k] -= result[i][k] * a;
			}

		}

	}

	for(size_t i = 0; i < _rows; i++) {
		
		if(instance[i][i] == 0) {
			throw std::runtime_error("Matrix is singular");
		}

		for(size_t j = 0; j < _rows; j++) {
			result[i][j] /= instance[i][i];
		}

	}

	return result;

}

void Matrix::identity() {

	if(_rows != _cols) {
		throw std::runtime_error("Can't create identity matrix of non-square matrix");
	}

	for(int i = 0; i < _rows; i++) {
		for(int j = 0; j < _rows; j++) {
			if(i == j) {
				_data[i][j] = 1;
			} else {
				_data[i][j] = 0;
			}
		}
	}

}

bool Matrix::regular() {

	try {
		inverse();
	} catch(std::exception) {
		return false;
	}

	return true;
}

void Matrix::fill_random() {

	for(size_t i = 0; i < _rows; i++) {
		for(size_t j = 0; j < _cols; j++) {
			_data[i][j] = (double) rand() / RAND_MAX;
		}
	}

}

Matrix linear_solve(Matrix A, Matrix b) {

	if(A.rows() != b.rows() || b.cols() != 1) {
		throw std::runtime_error("Matrix dimensions don't match for linear solve.");
	}

	Matrix C(A.rows(), A.cols() + 1);
	double q;

	for(int i = 0; i < A.rows(); i++) {
		for(int j = 0; j < A.cols(); j++) {
			C[i][j] = A[i][j];
		}
		C[i][A.cols()] = b[i][0];
	}

	for(int i = 0; i < C.rows(); i++) {

		int p = i + 1;

		while(C[i][i] == 0) {

			if(p == A.rows()) {
				break;
			}

			for(int j = 0; j < C.cols(); j++) {
				std::swap(C[i][j], C[p][j]);
			}

			p++;

		}

		if(C[i][i] == 0) {
			continue;
		}

		for(int j = i + 1; j < C.rows(); j++) {
			q = C[j][i] / C[i][i];
			for(int k = 0; k < C.cols(); k++) {
				C[j][k] = C[j][k] - q * C[i][k];
			}
		}

	}

	Matrix solution(A.cols(), 1);

	double s;

	for(int i = C.rows() - 1; i >= 0; i--) {
		s = C[i][C.cols() - 1];
		for(int j = A.cols() - 1; j >= i; j--) {
			s -= C[i][j] * solution[j][0];
		}
		if(C[i][i] == 0 && s != 0) {
			throw std::runtime_error("System is unsolvable");
		} else {
			solution[i][0] = s / C[i][i];
		}
	}

	return solution;

}

Vector::Vector(unsigned int rows) : Matrix(rows, 1) {}

std::ostream& operator<<(std::ostream& os, Matrix matrix)
{

	for(size_t i = 0; i < matrix.rows(); i++) {
		for(size_t j = 0; j < matrix.cols(); j++) {
			os << matrix._data[i][j] << '\t';
		}
		os << std::endl;
	}

    return os;
}