/**
 * Matrizenklasse
 */

#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <iostream>

class Matrix {
public:

	/* Init matrix specifying rows and cols number */
	Matrix(unsigned int, unsigned int);
	/* Get rows number */
	unsigned int rows() const;
	/* Get cols number */
	unsigned int cols() const;
	/* Access matrix elements, like A[1][2], counting from 0 */
	double* operator[](unsigned int);
	/* Matrix sum */
	Matrix operator+(Matrix);
	/* Matrix subtract */
	Matrix operator-(Matrix);
	/* Matrix multiplication */
	Matrix operator*(Matrix);
	/* Scalar multiplication */
	Matrix operator*(double);
	/* Matrix inverse if exists, otherwise throws exception */
	Matrix inverse();
	/* Get another instance of matrix. Don't know better solution than manual copy. :( */
	Matrix copy();
	/* Matrix transpose*/
	Matrix transpose();
	/* Setup matrix values for identity matrix */
	void identity();
	/* Fill with random values */
	void fill_random();
	/* Check whether matrix is regular */
	bool regular();
	/* Frobenius norm */
	double norm();

	double **_data;

protected:
	unsigned int _rows, _cols;
};

/* Simplified initialization (and element access in prospect) */
class Vector : public Matrix {
public:
	Vector(unsigned int);
};

/* Löse LGS */
Matrix linear_solve(Matrix, Matrix);

/* Output matrix into stream */
std::ostream& operator<<(std::ostream& os, Matrix matrix);

#endif